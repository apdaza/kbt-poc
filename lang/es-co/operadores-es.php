<?php
/**
*
*
*<ul>
* <li> MadeOpen <madeopensoftware.com></li>
*<li> Proyecto PNCAV</li>
*</ul>
*/

/**
* Language Operadores
*
* @package  clases
* @subpackage lang
* @author Alejandro Daza
* @version 2019.02
* @copyright SERTIC - MINTICS
*/
define('OPERADOR','Operador');
define('OPERADOR_NOMBRE','Nombre operador');
define('OPERADOR_SIGLA','Sigla');
define('ERROR_OPERADOR','** Debe Seleccionar un operador');

define('OPERADOR_CONTRATO_NRO', 'Numero del Contrato');
define('OPERADOR_CONTRATO_VALOR', 'Valor del Contrato');

?>
